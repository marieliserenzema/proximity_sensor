# Internet of things

Comme demandé, voici le rendu des 7 TPs organisés durant les cours
 
 [Rendu des tps](tps_rendu.md)

Puis la documentation de notre projet final

 [Projet Final](projet_final_doc.pdf)

 [Annexe](./Pics/annexes/)

Enfin voici la vidéo de démontration du projet final

 [Vidéo de l'affichage](./Pics/Affichage.mov)

 [Vidéo de simulation](./Pics/Simulation.MOV)


## Membre du projet

- PAJAK Alexandre
- DURAND Erwan
- RENZEMA Marie Lise
