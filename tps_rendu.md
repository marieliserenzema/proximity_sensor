# Exercice_1

- When blue button is released, the leds must lit in this order: orange, red, blue, green. 
- Whenb blue button is pushed, reverse order.

## Project_1/Core/Src/main.c
```c
  while (1)
  {


	if(HAL_GPIO_ReadPin(B1_GPIO_Port, B1_Pin)==GPIO_PIN_RESET) {
		for(int i=0;i<4;i++){
			HAL_GPIO_WritePin(ports[i], pins[i], GPIO_PIN_SET);
			HAL_Delay(100);
		}
		HAL_Delay(500);
		for(int i=0;i<4;i++){
			HAL_GPIO_WritePin(ports[i], pins[i], GPIO_PIN_RESET);
		}
		HAL_Delay(500);

	} else {

		for(int i=3;i>=0;i--){
			HAL_GPIO_WritePin(ports[i], pins[i], GPIO_PIN_SET);
			HAL_Delay(100);
		}
		HAL_Delay(500);
		for(int i=0;i<4;i++){
			HAL_GPIO_WritePin(ports[i], pins[i], GPIO_PIN_RESET);
		}
		HAL_Delay(500);
	}




    /* USER CODE END WHILE */
    MX_USB_HOST_Process();

    /* USER CODE BEGIN 3 */
  }
  ```
# Exercice_2

- Instanciate 2 timers:
    - 1st : with a 1 Hz Frequence
    - 2nd : with a 10 Hz Frequence

        Each time timer's interruption is called, a
        special led is toggled.

## In `Pinout & Configuration`

### `TIM6`
- Mode: Activated
- Configuration: 
    - Prescaler: 4199
    - Counter Period: 19999
- NVIC Settings :
    - TIM6 [...]: Enable

### `TIM7`
- Mode: Activated
- Configuration: 
    - Prescaler: 4199
    - Counter Period: 1999
- NVIC Settings :
    - TIM7 [...]: Enable

## In `Project_2_timer/Core/Src/main.c`

- Add :
    ```c
    /* USER CODE BEGIN 2 */
    HAL_TIM_Base_Start_IT(&htim6);
    HAL_TIM_Base_Start_IT(&htim7);
    /* USER CODE END 2 */
    ```

## In `Project_2_timer/Core/Src/stm32f4xx_it.c`

- Add :
```c
void TIM6_DAC_IRQHandler(void)
{
	/* USER CODE BEGIN TIM6_DAC_IRQn 0 */
	HAL_GPIO_TogglePin(LD4_GPIO_Port,LD4_Pin);
	/* USER CODE END TIM6_DAC_IRQn 0 */
	HAL_TIM_IRQHandler(&htim6);
	/* USER CODE BEGIN TIM6_DAC_IRQn 1 */

	/* USER CODE END TIM6_DAC_IRQn 1 */
}

/**
 * @brief This function handles TIM7 global interrupt.
 */
void TIM7_IRQHandler(void)
{
	/* USER CODE BEGIN TIM7_IRQn 0 */
	HAL_GPIO_TogglePin(LD5_GPIO_Port,LD5_Pin);

	/* USER CODE END TIM7_IRQn 0 */
	HAL_TIM_IRQHandler(&htim7);
	/* USER CODE BEGIN TIM7_IRQn 1 */

	/* USER CODE END TIM7_IRQn 1 */
}
```

## Logic analyzer
### Result:
![Image](./Pics/Exercice_2.jpg)


# Exercice_3

## Part 1
## 1. configuration 

- enable Usart2 and set as Asynchronous 
- run putty as root
- watch /dev/tty*

## 2. in Main.c

- add the followin code:
	```c
	/* USER CODE BEGIN PFP */
	void ecris_char(uint8_t car);
	void ecris_txt(uint8_t *txt);
	/* USER CODE END PFP */
	```
	```c
	/* USER CODE BEGIN 4 */
	void ecris_char(uint8_t car){
	HAL_UART_Transmit(&huart2,&car,1,1000);
	} void ecris_txt(uint8_t
	*
	txt){
	int i;
	for(i=0;i<strlen(txt);i++) {
	ecris_char(txt[i]);
	}
	} /* USER CODE END 4
	*/
	```
	```c
	int main(void)
	{
	/* USER CODE BEGIN 1 */
	uint8_t buffer[20]="A\n\r";
	/* USER CODE END 1 */
	…
	/* USER CODE BEGIN 2 */
	ecris_txt(buffer);
	/* USER CODE END 2 */
	```

	## Logic analyzer
	### Result:
	![Image](./Pics/Exercice_3.jpg)

## Part 2

- Add in `Main.c` the following code:
	```c
	/* USER CODE BEGIN PFP */
	void ecris_char(uint8_t car);
	void ecris_txt(uint8_t *txt);
	void lis_txt(char *txt);
	/* USER CODE END PFP */
	```
	```c
	int main(void)
	{
	/* USER CODE BEGIN 1 */
	char buffer[20]="Bonjour Arno !\n\r";
	/* USER CODE END 1 */
	…
	/* USER CODE BEGIN 2 */
	ecris_txt(buffer);
	/* USER CODE END 2 */
	/* Infinite loop */
	/* USER CODE BEGIN WHILE */
	while (1)
	{
	char reponse, retour[20];
	ecris_txt("Entre un chiffre : ");
	lis_txt(&reponse);
	sprintf(retour, "Choix=%c\n\r",reponse);
	ecris_txt(retour);
	/* USER CODE END WHILE */
	/* USER CODE BEGIN 3 */
	}
	/*
	```
	```c
	/* USER CODE BEGIN 4 */
	void ecris_char(uint8_t car){
	HAL_UART_Transmit(&huart2,&car,1,1000);
	} void ecris_txt(uint8_t
	*
	txt){
	int i;
	for(i=0;i<strlen(txt);i++) {
	ecris_char(txt[i]);
	}
	}
	void lis_txt(char *txt) {
	txt[0]='\0';
	while(!txt[0])
	HAL_UART_Receive(&huart2, txt, 1, 1000);
	} /* USER CODE END 4
	*/
	```

	### Result:
	![Image](./Pics/putty.PNG)

# Exercice_4

- Instanciate timer with a 1 Hz Frequence
    

## In `Pinout & Configuration`

### `TIM6`
- Mode: Activated
- Configuration: 
    - Prescaler: 4199
    - Counter Period: 19999
- NVIC Settings :
    - TIM6 [...]: Enable



## In main.c: 

- add includes:

	```c
	/* USER CODE BEGIN Includes */
	#include <stdio.h>
	#include <string.h>
	/* USER CODE END Includes */
	```
- External variable `counter` and temp variable `temp` :

	```c
	/* USER CODE BEGIN PV */
	int counter = 15;
	int temp = 0;
	/* USER CODE END PV */
	```
- add usart functions :

	```c
	/* USER CODE BEGIN PFP */
	void ecris_char(uint8_t car);
	void ecris_txt(uint8_t *txt);
	/* USER CODE END PFP */
	```

	```c
	/* USER CODE BEGIN 4 */
	void ecris_txt(uint8_t
			*
			txt){
		int i;
		for(i=0;i<strlen(txt);i++) {
			ecris_char(txt[i]);
		}
	}
	/* USER CODE END 4 */
	```
- start timer6 and turn off leds:

	```c
	/* USER CODE BEGIN 2 */
	HAL_TIM_Base_Start_IT(&htim6);
	HAL_GPIO_WritePin(LD6_GPIO_Port, LD6_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(LD5_GPIO_Port, LD5_Pin, GPIO_PIN_RESET);
	/* USER CODE END 2 */
	```

- infinit loop :

	```c
	/* Infinite loop */
	/* USER CODE BEGIN WHILE */
	while (1)
	{
		if (counter != temp){
			char retour[50];
			sprintf(retour, "%d seconds remaining.\n\r",counter);
			ecris_txt(retour);
			temp = counter;
		}
		HAL_Delay(500);
		/* USER CODE END WHILE */
		MX_USB_HOST_Process();

		/* USER CODE BEGIN 3 */
	}
	/* USER CODE END 3 */
	```

## In stm32f4xx_it.c:

- set global variable:

	```c
	/* USER CODE BEGIN PV */
	extern int counter;
	/* USER CODE END PV */
	```
- Fill Timer6 IRQ function :

	```c
	void TIM6_DAC_IRQHandler(void)
	{
	/* USER CODE BEGIN TIM6_DAC_IRQn 0 */
		counter -= 1;
		HAL_GPIO_TogglePin(LD6_GPIO_Port,LD6_Pin);
		if (counter == 0){
			HAL_GPIO_TogglePin(LD5_GPIO_Port,LD5_Pin);
			counter = 15;
		}
	/* USER CODE END TIM6_DAC_IRQn 0 */
	HAL_TIM_IRQHandler(&htim6);
	/* USER CODE BEGIN TIM6_DAC_IRQn 1 */

	/* USER CODE END TIM6_DAC_IRQn 1 */
	}
	```

- Result :

	![Image](./Pics/putty1.PNG)


# Exercice_5

## 1. Configuration
### In `Pinout & Configuration`:

### `TIM5`
- Mode: Activated
- Configuration: 
	- Prescaler: 1679
	- Counter Period: 999
- NVIC Settings :
	- TIM5 [...]: Enable

## In `Connectivity`
### `USART2`
- Mode Asynchrounous


## 2. Programation

- ## In `Main.c`

	- Include `stdio;h`
		```c
		/* USER CODE BEGIN Includes */
		#include <stdio.h>
		/* USER CODE END Includes */
		```
	- Add global variable `duty`
		```c
		/* USER CODE BEGIN PV */
		int duty = 50;
		/* USER CODE END PV */
		```
	- Init function
		```c
		/* USER CODE BEGIN PFP */
		void ecris_char(uint8_t car);
		void ecris_txt(uint8_t *txt);
		void lis_txt(char *txt);
		/* USER CODE END PFP */
		```
	- Start and Init at 50Hz
		```c
		/* USER CODE BEGIN 2 */
		HAL_TIM_PWM_Start(&htim5, TIM_CHANNEL_2);
		__HAL_TIM_SET_COMPARE(&htim5, TIM_CHANNEL_2, duty);
		/* USER CODE END 2 */
		```
	- Add in Infinit loop:
		```c
		/* Infinite loop */
		/* USER CODE BEGIN WHILE */
		while (1)
			{
				char message[50];
				int value;
				char reponse;
				lis_txt(&reponse);
				char reponseTxt[10];
				sprintf(reponseTxt, "%c\n\r", reponse);
				ecris_txt(reponseTxt);
				if ((reponse>='0' && reponse <= '9') || reponse == 'A'){
					if (reponse == 'A'){
						value = 10;
					} else {
						value = reponse - '0';
					}
					duty = duty + (value * 5);
					sprintf(message, "Duty-cycle : %d\n\r", duty);
					ecris_txt(message);
					__HAL_TIM_SET_COMPARE(&htim5, TIM_CHANNEL_2, duty);
					duty = 50;
				}
		/* USER CODE END WHILE */
		```

		- Add functions :
		```c
		/* USER CODE BEGIN 4 */
		void ecris_char(uint8_t car){
			HAL_UART_Transmit(&huart2,&car,1,1000);
		}
		void ecris_txt(uint8_t *txt){
			int i;
			for(i=0;i<strlen(txt);i++) {
				ecris_char(txt[i]);
			}
		}
		void lis_txt(char *txt) {
			txt[0]='\0';
			while(!txt[0])
				HAL_UART_Receive(&huart2, txt, 1, 1000);
		}
		/* USER CODE END 4 */
		```
## Result 
![image](./Pics/project_5_pwm.png)

# Exercice_6

## 1. Configuration
### In `Pinout & Configuration`:

### `ANALOG ADC1`
- Mode: `IN1`

- ADC_Settings: Resolution 12 bits

## In `Connectivity`
### `USART2`
- Mode Asynchrounous

## Add sprintf Float:
![printf](./Pics/project_6_setup.png)

## 2. Programmation
###  In `Main.c`
	- Add includes:

	```c
	/* USER CODE BEGIN Includes */
	#include <stdio.h>
	/* USER CODE END Includes */
	```
	- In Infinit loop:

		```c
		/* USER CODE BEGIN WHILE */
		while (1)
		{
			uint32_t adcData;
			uint32_t oldValue = 0;
			int pourcent;
			float volt;
			char message [50];

			HAL_ADC_Start(&hadc1);
			HAL_ADC_PollForConversion(&hadc1, 100);
			adcData = HAL_ADC_GetValue(&hadc1);
			HAL_ADC_Stop(&hadc1);

			if (!proche(adcData,oldValue)){
				pourcent =(adcData*100)/4095;
				volt = (adcData * 3.0F)/4095.0F;
				sprintf(message,"Valeur ADC: %X -> pourc = %d %% -> V = %.1f Volts \n\r",adcData,pourcent,volt);
				ecris_txt(message);
				oldValue = adcData;
			}
			HAL_Delay(200);
			/* USER CODE END WHILE */
		```

	- Add functions:

		```c
		/* USER CODE BEGIN 4 */
		#define delta 20
		int proche(uint32_t x, uint32_t y) {
		int dx=x-y; dx=(dx<0)?-dx:dx; return dx<delta;
		}
		void ecris_char(uint8_t car){
			HAL_UART_Transmit(&huart2,&car,1,1000);
		}
		void ecris_txt(uint8_t *txt){
			int i;
			for(i=0;i<strlen(txt);i++) {
				ecris_char(txt[i]);
			}
		}
		/* USER CODE END 4 */
		```

## Result:

![result](./Pics/projet_6_result.png)

# Exercice_7

## 1. Configuration
### In `Pinout & Configuration`:
### `TIM6`
- Mode: Activated
- Configuration: 
    - Prescaler: 0
    - Counter Period: 3817
- NVIC Settings :
    - TIM6 [...]: Enable

### `Analog`
- DAC OUT 1 configuration tick

## 2. Programation

- ## In `Main.c`
```c
/* USER CODE BEGIN PV */
extern int mode;
/* USER CODE END PV */
```
```c
  /* USER CODE BEGIN 2 */
  HAL_TIM_Base_Start_IT(&htim6);
  HAL_DAC_Start(&hdac, DAC1_CHANNEL_1);
  /* USER CODE END 2 */
```
```c
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
	// button check:
	if(HAL_GPIO_ReadPin(B1_GPIO_Port, B1_Pin)==GPIO_PIN_SET){
		if (mode == 3){
			mode = 0;
		} else {
			mode += 1;
		}
		while(HAL_GPIO_ReadPin(B1_GPIO_Port, B1_Pin)==GPIO_PIN_SET)
			HAL_Delay(50);
	}



    /* USER CODE END WHILE */
```
- ## In `stm32f4xx_it.c`

```c
/* USER CODE BEGIN PV */
int counter;
int mode;
/* USER CODE END PV */
```

```c
	/* USER CODE BEGIN TIM6_DAC_IRQn 0 */
	counter += 1;
	if (counter%50 == 0){
		counter =0;
	}
	// if mode 1 square signal with clock
	if (mode == 1) {
		if (counter < 25){
			HAL_DAC_SetValue(&hdac, DAC_CHANNEL_1, DAC_ALIGN_12B_L, 2482);
		} else {
			HAL_DAC_SetValue(&hdac, DAC_CHANNEL_1, DAC_ALIGN_12B_L, 1241);
		}
	}
	// if mode 2 saw-toothed signal:
	else if (mode == 2){
		int value = counter * 1241 / 50 + 1241;
		HAL_DAC_SetValue(&hdac, DAC_CHANNEL_1, DAC_ALIGN_12B_L, value);
	}
	// if mode 3 triangle signal:
	else if (mode == 3){
		if (counter <= 25){
			int value = 1241/25 * counter + 1241;
			HAL_DAC_SetValue(&hdac, DAC_CHANNEL_1, DAC_ALIGN_12B_L, value);
		}else{
			int value = (-1241/25) * counter + 3723;
			HAL_DAC_SetValue(&hdac, DAC_CHANNEL_1, DAC_ALIGN_12B_L, value);
		}
	}
	/* USER CODE END TIM6_DAC_IRQn 0 */
```

## Result:

### Mode_0
![mode_0](./Pics/mode0.jpg)
### Mode_1
![mode_1](./Pics/mode_1.jpg)
### Mode_2
![mode_2](./Pics/mode_2.jpg)
### Mode_3
![mode_3](./Pics/mode_3.jpg)